<?php

namespace Drupal\streamlike_oembed\Service;

/**
 * Streamlike Oembed Main Service
 */
class StreamlikeOembed {

  /**
   * Get the streamlike media id to embed
   * This is only supported for entity canonical routes
   */
  public function getMediaId() {

    // Only evaluate if on an entity canonical route
    $route = \Drupal::routeMatch();
    $route_parts = explode(".",$route->getRouteName()); //ksm($route_parts);
    if ( count($route_parts)!=3 || $route_parts[0]!="entity" || $route_parts[2]!="canonical" ) return FALSE;

    // Get module config
    $config = \Drupal::config('streamlike_oembed.settings');

    // Get sources routes|field_names as an array
    $sources = array_map('trim', explode("\n", $config->get('discover_media_id_source'))); 

    // Scan sources
    foreach ( $sources as $source ) {

      // Put route | field_name into an array
      $parts = explode("|",$source); 

      // If source route matches current
      if ( $parts[0]==$route->getRouteName() ) {

        // Get entity for route
        $entity = $this->getPageEntity($route); 

        // Check if field exists on entity 
        if ( $entity ) {

          // Get name of field to get
          $field_name = $parts[1];

          // Is field present on entity ?
          if ($entity->hasField($field_name)) {

            // Get value depending on field type
            $field_type = $entity->get($field_name)->getFieldDefinition()->getType();
            if ( $field_type=="vpx_media_field" ) { // vpx
              return $entity->{$field_name}->emid;
            } else { // Default
              return $entity->{$field_name}->value;
            }

          }

        }  

      }

    }

    return NULL;
  }

  /**
   * Gets the title of the entity containing the media to embed
   * 
   * @param $route
   * 
   * @return String|null
   */
  public function getMediaTitle($route=null) {
    // Get entity for $route
    $entity = $this->getPageEntity($route);
    // Get label of entity
    if ( $entity ) {
      if ( null!==$entity->label() ) return $entity->label();
    }
    // No title found
    return null;
  }

  /**
   * Gets the page main entity for a route
   *
   * @param $route
   * 
   * @return \Drupal\Core\Entity\EntityInterface
   *   Current page main entity.
   */
  public function getPageEntity($route=null) {
    if (empty($route)) $route = \Drupal::routeMatch();
    $types = array_keys(\Drupal::entityTypeManager()->getDefinitions());
    $params = $route->getParameters()->all();
    foreach ($types as $type) {
      if (!empty($params[$type])) {
        return $params[$type];
      }
    }
    return NULL;
  }

  /**
   * Checks if a value is a valid media id,
   * eg: 6043e6f2029ac621
   * 
   * @param String $media_id
   * 
   * @return Boolean
   */
  public function isValidMediaId($media_id) {
    // Must no be empty
    if ( empty($media_id) ) return FALSE;
    // Must be a string
    if ( !is_string($media_id) ) return FALSE;
    // Must be 16 digits 
    if ( strlen($media_id)!=16 ) return FALSE;
    // All checks passed: media_id is valid
    return TRUE;
  }

}
