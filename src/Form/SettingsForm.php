<?php

namespace Drupal\streamlike_oembed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Streamlike oEmbed settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'streamlike_oembed_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['streamlike_oembed.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // ---- oEmbed Discovery ----

    $form['discover'] = [
      '#type' => 'details',
      '#title' => $this->t('oEmbed Discovery'),
      '#open' => TRUE,
    ];
    $form['discover']['discover_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate discovery mechanism'),
      '#default_value' => $this->config('streamlike_oembed.settings')->get('discover_active'),
    ];
    $description = [];
    $description[] = $this->t('Enter canonical entities route names and fields holding the streamlike media id, as: entity.entity_type_id.canonical|field_name');
    $description[] = $this->t('Discovery code will only be added if any media _id is found.');
    $form['discover']['discover_media_id_source'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Entity route / Field to get Streamlike media ID from'),
      '#default_value' => $this->config('streamlike_oembed.settings')->get('discover_media_id_source'),
      '#description' => implode("<br>",$description),
    ];    
    $form['discover']['discover_maxwidth'] = [
      '#type' => 'textfield',
      '#size' => 8,
      '#title' => $this->t('Media max width'),
      '#default_value' => $this->config('streamlike_oembed.settings')->get('discover_maxwidth'),
      '#description' => $this->t("Media max requested width, in pixels")
    ];
    $form['discover']['discover_maxheight'] = [
      '#type' => 'textfield',
      '#size' => 8,
      '#title' => $this->t('Media max height'),
      '#default_value' => $this->config('streamlike_oembed.settings')->get('discover_maxheight'),
      '#description' => $this->t("Media max requested height, in pixels")
    ];

    // ---- Streamlike settings ----

    $form['streamlike'] = [
      '#type' => 'details',
      '#title' => $this->t('Streamlike Settings'),
      '#open' => TRUE,
    ];
    $form['streamlike']['oembed_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Streamlike oEmbed endpoint'),
      '#default_value' => $this->config('streamlike_oembed.settings')->get('oembed_endpoint'),
      '#description' => $this->t("No trailing slash")."<br/>".$this->t("Defaults to https://cdn.streamlike.com/oembed")
    ];
    $form['streamlike']['media_provider'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Streamlike media provider'),
      '#default_value' => $this->config('streamlike_oembed.settings')->get('media_provider'),
      '#description' => $this->t("The domain serving the media, eg MEDIA_PROVIDER/play?med_id=...")."<br/>".$this->t("Defaults to https://cdn.streamlike.com")."<br/>".$this->t("No trailing slash")
    ];
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('streamlike_oembed.settings')
      ->set('discover_active', $form_state->getValue('discover_active'))
      ->set('discover_media_id_source', $form_state->getValue('discover_media_id_source'))
      ->set('discover_maxwidth', $form_state->getValue('discover_maxwidth'))
      ->set('discover_maxheight', $form_state->getValue('discover_maxheight'))
      ->set('oembed_endpoint', $form_state->getValue('oembed_endpoint'))
      ->set('media_provider', $form_state->getValue('media_provider'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
